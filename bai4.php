<?php
$result = "";
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(isset($_POST['soThuNhat'])){
        $num1 = $_POST['soThuNhat'];
    }
    if(isset($_POST['soThuHai'])){
        $num2 = $_POST['soThuHai'];
    }
    if(isset($_POST['operation'])){
        $operator = $_POST['operation'];
        switch($operator) {
            case '+':
                $result = $num1 + $num2;
                break;
            case '-':
                $result = $num1 - $num2;
                break;
            case '*':
                $result = $num1 * $num2;
                break;
            case '/':
                $result = $num1 / $num2;
                break;
            default:
                echo "operation khong hop le 123";
        }
    }
    
}


?>
<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <meta charset="UTF-8"/>
    <title>Bai 4</title>
    </head>
    <body>
        <div class="container">
            <form action="bai4.php" method="POST">
                <div>
                    Số thứ 1
                </div>
                <div>
                    <input type="text" name="soThuNhat" />
                    <button type="submit" name="operation" value="+">Cộng</button>
                    <button type="submit" name="operation" value="-">Trừ</button>
                </div>
                <div>
                    Số thứ 2
                </div>
                <div>
                    <input type="text" name="soThuHai" />
                    <button type="submit" name="operation" value="*">Nhân</button>
                    <button type="submit" name="operation" value="/">Chia</button>
                </div>
                <div>
                    Kết quả
                    <?php
                        echo "Result: " . $result;
                    ?>
                </div>
                <div>
                </div>
            </form>
        </div>
    </body>
</html>